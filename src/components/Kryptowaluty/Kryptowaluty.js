import React from 'react';
import './Kryptowaluty.css';
import 'bootstrap/dist/css/bootstrap.css';
import logoBlack from '../../images/logo.png';
import logoWhite from '../../images/logo-white.png';
import Depozyt from '../Depozyt/Depozyt';
import Wyplata from '../Wyplata/Wyplata';
import Kontakt from '../Kontakt/Kontakt';
import {ThemeProvider} from "styled-components";
import { GlobalStyles } from '../StylGlobalny/StylGlobalny.js';
import { lightTheme, darkTheme } from "../Motyw/Motyw.js"
import { useState } from "react";
import { Link } from 'react-router-dom'


function Kryptowaluty() {
  const [depozytShow, setDepozytShow] = React.useState(false);
  const [wyplataShow, setWyplataShow] = React.useState(false);
  const [kontaktShow, setKontaktShow] = React.useState(false);
  const [theme, setTheme] = useState('light');
  const [logo, setLogo] = useState('light');
  const themeToggler = () => {
    theme === 'light' ? setTheme('dark') : setTheme('light')
    logo === 'light' ? setLogo('dark') : setLogo('light')
  }

  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <>
      <GlobalStyles/>
    
        <Depozyt
            show={depozytShow}
            onHide={() => setDepozytShow(false)}
        />
        <Wyplata
            show={wyplataShow}
            onHide={() => setWyplataShow(false)}
        />
        <Kontakt
            show={kontaktShow}
            onHide={() => setKontaktShow(false)}
        />
        <div className="custom-control custom-switch">
          <input onClick={themeToggler} type="checkbox" className="custom-control-input" id="customSwitch1"/>
          <label className="custom-control-label" for="customSwitch1">Tryb nocny</label>
        </div>
        <img className="logo" src={logo === 'light' ? logoBlack : logoWhite } alt="currex"/>

        <div className="left-bar">
          <div>
            <h6 className="text">Witaj,  !</h6>
            <h6 className="text">Saldo: </h6>     
          </div>
          <button className="btn btn-warning" onClick={() => setDepozytShow(true)}>Depozyt</button>
          <button className="btn btn-warning" onClick={() => setWyplataShow(true)}>Wypłata</button>
          <div className="menu">
          <Link to='/index'>
              <button className="btn btn-danger">Strona główna</button>
            </Link>
            <br></br>
            <button className="btn btn-danger">Analityka</button>
            <br></br>
            <button className="btn btn-danger">Portfel</button>
            <br></br>
            <Link to='/kryptowaluty'>
              <button className="btn btn-danger">Kryptowaluty</button>
            </Link>
          </div>
          <button className="btn-contact btn btn-danger" onClick={() => setKontaktShow(true)}>Kontakt</button>
        </div>
        <div className="searchBox">
          <input type="search" placeholder="Wyszukaj"/>
          <button type="button" className="btn btn-danger">Wyszukaj</button>
        </div>
        <div className="container">  
            <div className="row">
           
            </div>
        </div>
        <footer>
          <p>© 2021 CurrEX inc. All rights reserved. Terms and Conditions</p>
        </footer>
      </>
    </ThemeProvider>
  );
  
  
}


export default Kryptowaluty;
