import './Wyplata.css';
import 'bootstrap/dist/css/bootstrap.css';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup"
import FormControl from "react-bootstrap/FormControl"
import FormCheck from "react-bootstrap/FormCheck"

function Wyplata(props) {
    return (
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Wypłata
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Wypłata salda w PLN</h5>
          <p>Twoje saldo: 99999</p>
          <InputGroup className="mb-3">
            <FormControl placeholder="Ilość..." />
          </InputGroup>
          <InputGroup className="mb-3">
            <FormControl placeholder="Numer konta..." />
          </InputGroup>
          <InputGroup>          
            <FormControl type="password" placeholder="Hasło..." />
          </InputGroup>
          <FormCheck type="checkbox" label="Zgadzam się z regulaminem" />
        </Modal.Body>
        <Modal.Footer>
          <Button  className="btn btn-danger" onClick={props.onHide}>Wypłać</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  export default Wyplata;