import React from 'react';
import './Index.css';
import 'bootstrap/dist/css/bootstrap.css';
import logoBlack from '../../images/logo.png';
import logoWhite from '../../images/logo-white.png';
import Waluta from '../Waluta/Waluta.js';
import Depozyt from '../Depozyt/Depozyt';
import Wyplata from '../Wyplata/Wyplata';
import Kontakt from '../Kontakt/Kontakt';
import {ThemeProvider} from "styled-components";
import { GlobalStyles } from '../StylGlobalny/StylGlobalny.js';
import { lightTheme, darkTheme } from "../Motyw/Motyw.js"
import { useState, useContext, useEffect } from "react";
import {MyContext} from '../Context/MyContext';
import Login from '../Logowanie/Logowanie';
import Register from '../Rejestracja/Rejestracja';
import {useAuth0 } from '@auth0/auth0-react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Auth0Client } from '@auth0/auth0-spa-js';


const Index = () => {

  const [userMetadata, setUserMetadata] = useState(null);

  useEffect(()=>{
    const getUserMetadata = async () => {
      const domain = "dev-byhvouyq.eu.auth0.com";
  
      try {
        const accessToken = await getAccessTokenSilently({
          audience: `https://${domain}/api/v2/`,
          scope: "read:current_user",
        });
  
        const userDetailsByIdUrl = `https://${domain}/api/v2/users/${user.sub}`;
  
        const metadataResponse = await fetch(userDetailsByIdUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
  
        const { user_metadata } = await metadataResponse.json();
  
        setUserMetadata(user_metadata);
      } catch (e) {
        console.log(e.message);
      }
    };
  
    getUserMetadata();

    getRate();
    //getBalance(user.sub);
    //insertUser();
  },[]);
  

  

  const [data, setData] = useState([]);


  const getBalance = async(user_id) => {

    const form_data = new FormData();
    form_data.append('user_id', user_id);

    let res = await axios.post('http://localhost/currex/balance_select.php', form_data);
    const tempData = res.data;
    setData(tempData);
   
  }

  console.log(JSON.stringify(data));



  const {logout} = useAuth0();
  const {user, isAuthenticated, isLoading, getAccessTokenSilently } = useAuth0();
  const [depozytShow, setDepozytShow] = React.useState(false);
  const [wyplataShow, setWyplataShow] = React.useState(false);
  const [kontaktShow, setKontaktShow] = React.useState(false);
  const [theme, setTheme] = useState('light');
  const [logo, setLogo] = useState('light');
  const themeToggler = () => {
    theme === 'light' ? setTheme('dark') : setTheme('light')
    logo === 'light' ? setLogo('dark') : setLogo('light')
}
 //Currency rate

 const [ratesList, setRatesList] = useState([]);



 const API_KEY = 'bee52fe39ff88e63f21b';
 const ratesArray = ['EUR_PLN','USD_PLN'];
 
 const getRate = async() =>{
   const res = await axios.get(`https://free.currconv.com/api/v7/convert?q=${ratesArray.join(',')}&compact=ultra&apiKey=${API_KEY}`);
   const rates = res.data;
   const ratesTemp = [];
   for (const [symbol, rate] of Object.entries(rates)) {
     ratesTemp.push({ symbol, rate });
   }
   setRatesList(ratesTemp);
 };

 const insertUser = async(user_id) => {

  const form_data = new FormData();
  form_data.append('user_id', user_id);

  const res = await axios.post('http://localhost/currex/insert_user.php', form_data);
  
  console.log(res);
}

    if(!user){
      return <Login />
    }
    return (
      
      <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
        <>
        <GlobalStyles/>
      
          <Depozyt
              show={depozytShow}
              onHide={() => setDepozytShow(false)}
          />
          <Wyplata
              show={wyplataShow}
              onHide={() => setWyplataShow(false)}
          />
          <Kontakt
              show={kontaktShow}
              onHide={() => setKontaktShow(false)}
          />
          <div onLoad={insertUser(user.sub)}></div>
          <div onLoad={getBalance(user.sub)}></div>
          
          <div className="custom-control custom-switch">
            <input onClick={themeToggler} type="checkbox" className="custom-control-input" id="customSwitch1"/>
            <label className="custom-control-label" htmlFor="customSwitch1">Tryb nocny</label>
          </div>
          <img className="logo" src={logo === 'light' ? logoBlack : logoWhite } alt="currex"/>
  
          <div className="left-bar">
            <div>
              <img src={user.picture} />
              <h6 className="text">Witaj, {user.name}!</h6>
              <button onClick={() => logout()} type="submit"className="btn btn-danger">Wyloguj</button>
              <h6 className="text">Saldo: {data.map((el)=>el.PLN)} zł</h6>     
            </div>
            <button className="btn btn-warning" onClick={() => setDepozytShow(true)}>Depozyt</button>
            <button className="btn btn-warning" onClick={() => setWyplataShow(true)}>Wypłata</button>
            <div className="menu">
              <button className="btn btn-danger">Strona główna</button>
              <br></br>
              <button className="btn btn-danger">Analityka</button>
              <br></br>
              <button className="btn btn-danger">Portfel</button>
              <br></br>
              <Link to='/kryptowaluty'>
              <button className="btn btn-danger">Kryptowaluty</button>
            </Link>
            </div>
            <button className="btn-contact btn btn-danger" onClick={() => setKontaktShow(true)}>Kontakt</button>
          </div>
          <div className="searchBox">
            <input type="search" placeholder="Wyszukaj"/>
            <button type="button" className="btn btn-danger">Wyszukaj</button>
          </div>
          <div className="container">  
            <div className="row">  
            <Waluta ratesList={ratesList}/>
            </div>
          </div>
          <footer>
            <p>© 2021 CurrEX inc. All rights reserved. Terms and Conditions</p>
          </footer>
        </>
      </ThemeProvider>
    )
  
  
  
  
}


export default Index;
