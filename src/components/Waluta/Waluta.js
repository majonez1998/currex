import './Waluta.css';
import 'bootstrap/dist/css/bootstrap.css';


const Waluta = ({ratesList}) => {
  return (
  <>
    {ratesList.map((element)=>{
        return(
          <div className="col-5">
            <div className="component">
                <h5>{element.symbol}</h5>
                <h5 className="price">{element.rate}</h5>
                <button className="btn btn-danger">Analityka</button>
            </div>   
          </div>
        )
      })}
  </>

  );
}


export default Waluta;

