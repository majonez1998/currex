import './Logowanie.css';
import 'bootstrap/dist/css/bootstrap.css';
import logo from '../../images/logo.png';
import { Link, Redirect } from 'react-router-dom';
import React, {useContext, useState } from 'react';
import {useAuth0} from '@auth0/auth0-react';
import Profile from '../Profile/Profile.js';
import Index from '../Index/Index.js';


const Logowanie = () =>{
 
    const{user, loginWithRedirect} = useAuth0();
    
  
  
    if(user){
      return <Redirect push to="/Index" />;
    }
 
    return (
      <>

      

        <div className="content">
            <h2>Zaloguj się do Currex</h2>
            {/* <p>Wprowadź login i hasło, aby się zalogować.</p> 
          
            <div className="form-group">
              <label for="exampleInputLogin1">Login</label>
              <input name="username" type="text" className="form-control" id="exampleInputLogin1" aria-describedby="emailHelp" placeholder="Wprowadź login" />
            </div>
            <div className="form-group">
              <label for="exampleInputPassword1">Hasło</label>
              <input name="password" type="password" className="form-control" id="exampleInputPassword1" placeholder="Wprowadź hasło" />
            </div> */}
            
            <button onClick={() => loginWithRedirect()} type="submit"className="btn btn-danger">Zaloguj</button>
            

          

        <Profile />
        </div>
        <div className="right-bar">
          <div className="container">
            <h6 className="text">Nie masz jeszcze konta? </h6>
            <Link to="/rejestracja">
              <button className="btn btn-warning signup" type="button" >Zarejestruj się</button>
            </Link>
          </div>
          <img src={logo} alt="currex"/>
        </div>
      </>
    
      );
  

}

export default Logowanie;
