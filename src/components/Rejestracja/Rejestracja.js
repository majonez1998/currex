import './Rejestracja.css';
import 'bootstrap/dist/css/bootstrap.css';
import logo from '../../images/logo.png';
import {Link} from 'react-router-dom';
import React, {Component} from 'react';
import axios from 'axios';

class Rejestracja extends Component{
    state ={
        login:"",
        password:"",
        email:"",
        phone:""

    };
    handleAddLogin= async e =>{      
        await this.setState({
            login: e.target.value
        })
    }
    handleAddPass= async e =>{      
        await this.setState({
            password: e.target.value
        })
    }
    handleAddEmail= async e =>{      
        await this.setState({
            email: e.target.value
        })
    }
    handleAddTel= async e =>{      
        await this.setState({
            phone: e.target.value
        })
    }
    handleSubmit = e =>{

        if(this.state.login ===''){
            alert("Login wpisz");
        }
        if(this.state.password ===''){
            alert("Hasło wpisz");
        }
        if(this.state.email===''){
            alert("Email wpisz");
        }
        if(this.state.phone ===''){
            alert("Telefon wpisz");
        }
        else{
            e.preventDefault();
            console.log(this.state.login);
            console.log(this.state.password);
            let formData = new FormData();
            formData.append("login", this.state.login);
            formData.append("email", this.state.email);
            formData.append("phone", this.state.phone);
            formData.append("password", this.state.password);
            const url = "https://michalmajecki2.000webhostapp.com/currex-rej.php";
            axios.post(url,formData).then(res=>alert(res.data)).catch(err=>console.log(err));
            
            
        }

        
    }
    render(){
        return (
            <>
              <div className="content">
                  <h2>Zarejestruj się w Currex</h2>
                  <p>Wprowadź poniżej dane, aby się zarejestrować.</p>
                  
                  <div className="form-group">
                      <label htmlFor="exampleInputLogin1">Login</label>
                      <input onChange={this.handleAddLogin} type="text" className="form-control" id="login" aria-describedby="emailHelp" placeholder="Wprowadź login" required="required" />
                  </div>
                  <div className="form-group">
                      <label htmlFor="exampleInputEmail1">Email</label>
                      <input onChange={this.handleAddEmail} type="email" className="form-control" id="mail" placeholder="Wprowadź email" required="required" />
                  </div>
                  <div className="form-group">
                      <label htmlFor="exampleInputTel1">Telefon</label>
                      <input onChange={this.handleAddTel} type="tel" className="form-control" id="phone" placeholder="Wprowadź telefon" required="required" />
                  </div>
                  <div className="form-group">
                      <label htmlFor="exampleInputPassword1">Hasło</label>
                      <input onChange={this.handleAddPass} type="password" className="form-control" id="password" placeholder="Wprowadź hasło" required="required" />
                  </div>
                  
                  <button onClick={this.handleSubmit} type="submit" className="btn btn-danger">Zarejestruj</button>
                
          
              </div>
              <div className="right-bar">
                  <div className="container">
                      <h6 className="text">Masz już konto?</h6>
                      <Link to="/">
                          <button className="btn btn-warning signup" type="button" > Zaloguj się</button>
                      </Link>
                  </div>
                  <img src={logo} alt="currex"/>
              </div>

             
            </>
                    
            );
    }
}

export default Rejestracja;
