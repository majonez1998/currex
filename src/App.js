import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Logowanie from './components/Logowanie/Logowanie.js';
import Rejestracja from './components/Rejestracja/Rejestracja.js';
import Index from './components/Index/Index.js';
import { BrowserRouter, Route, Switch} from "react-router-dom";
import React, {Component} from 'react';
import Kryptowaluty from './components/Kryptowaluty/Kryptowaluty.js'
import Historia from './components/Historia/Historia'
class App extends Component{
  render(){
    return (
      <>
      <BrowserRouter>
         <Switch>
          <Route exact path="/" component={Logowanie} />
          <Route path="/rejestracja" component={Rejestracja} />
          <Route path="/index" component={Index} />
          <Route path="/kryptowaluty" component={Kryptowaluty} />
          <Route path="/historia" component={Historia} />
        </Switch>
        </BrowserRouter>   
      </>
    );
  }
}


export default App;
